# Emulators and Roms

## Directory Structure

OS 4 FINAL 27 has the following directory structure for ROMs and Extras. 

```
/media
├── nfsg
├── nfsl
├── sd
│   ├── bios
│   ├── cheats
│   │   └── FinalBurn Neo
│   ├── coreconfig
│   ├── dats
│   ├── gameconfig
│   ├── remaps
│   ├── roms
│   │   ├── amiga
│   │   ├── amigacd
│   │   ├── amstradcpc
│   │   ├── arcade
│   │   │   ├── fbneo
│   │   │   ├── mame
│   │   │   └── naomi
│   │   ├── atari2600
│   │   ├── atari7800
│   │   ├── c64
│   │   ├── dreamcast
│   │   ├── gba
│   │   ├── kodi
│   │   ├── lightgun
│   │   │   ├── dreamcast
│   │   │   ├── fbneo
│   │   │   ├── mastersystem
│   │   │   ├── megadrive
│   │   │   ├── naomi
│   │   │   ├── nes
│   │   │   ├── psx
│   │   │   ├── segacd
│   │   │   └── snes
│   │   ├── mastersystem
│   │   ├── megadrive
│   │   ├── msx
│   │   ├── n64
│   │   ├── neocd
│   │   ├── neogeo
│   │   ├── nes
│   │   ├── ngp
│   │   ├── pc
│   │   ├── pcengine
│   │   ├── pcenginecd
│   │   ├── ports
│   │   ├── psx
│   │   ├── scripts
│   │   ├── sega32x
│   │   ├── segacd
│   │   ├── sg1000
│   │   ├── sgb
│   │   ├── snes
│   │   ├── x68000
│   │   └── zxspectrum
│   ├── saves
│   │   ├── amiga
│   │   ├── amigacd
│   │   ├── amstradcpc
│   │   ├── arcade
│   │   │   ├── fbneo
│   │   │   │   └── fbneo
│   │   │   ├── mame
│   │   │   └── naomi
│   │   ├── atari2600
│   │   ├── atari7800
│   │   ├── c64
│   │   ├── dreamcast
│   │   ├── gba
│   │   ├── lightgun
│   │   │   ├── dreamcast
│   │   │   ├── fbneo
│   │   │   ├── mastersystem
│   │   │   ├── megadrive
│   │   │   ├── naomi
│   │   │   ├── nes
│   │   │   ├── psx
│   │   │   ├── segacd
│   │   │   └── snes
│   │   ├── mastersystem
│   │   ├── megadrive
│   │   ├── msx
│   │   ├── n64
│   │   ├── neocd
│   │   ├── neogeo
│   │   ├── nes
│   │   ├── ngp
│   │   ├── pc
│   │   ├── pcengine
│   │   ├── pcenginecd
│   │   ├── ports
│   │   ├── psx
│   │   ├── scripts
│   │   ├── sega32x
│   │   ├── segacd
│   │   ├── sg1000
│   │   ├── sgb
│   │   ├── snes
│   │   ├── x68000
│   │   └── zxspectrum
│   └── screenshots
│       ├── amiga
│       ├── amigacd
│       ├── amstradcpc
│       ├── arcade
│       │   ├── fbneo
│       │   ├── mame
│       │   └── naomi
│       ├── atari2600
│       ├── atari7800
│       ├── c64
│       ├── dreamcast
│       ├── gba
│       ├── lightgun
│       │   ├── dreamcast
│       │   ├── fbneo
│       │   ├── mastersystem
│       │   ├── megadrive
│       │   ├── naomi
│       │   ├── nes
│       │   ├── psx
│       │   ├── segacd
│       │   └── snes
│       ├── mastersystem
│       ├── megadrive
│       ├── msx
│       ├── n64
│       ├── neocd
│       ├── neogeo
│       ├── nes
│       ├── ngp
│       ├── pc
│       ├── pcengine
│       ├── pcenginecd
│       ├── ports
│       ├── psx
│       ├── scripts
│       ├── sega32x
│       ├── segacd
│       ├── sg1000
│       ├── sgb
│       ├── snes
│       ├── x68000
│       └── zxspectrum
├── usb1
└── usb2
```

## Emulators

OS 4 Final 27 comes with the following [libretro](https://www.libretro.com/) cores installed to the location `/opt/retroarch/core`. Roms must be placed within `/roms/[system]` on SD card or USB media. The corresponding libretro core will automatically be choosen based on this path. Any subfolders that you create will be shown within the UI.

| System       | Libretro Core       | Core Version |
|--------------|---------------------|--------------|
| amiga        | puae                | v4.9.1       |
| amigacd      | puae                | v4.9.1       |
| amstradpc    | cap32               | v4.2.0       |
| arcade/fbneo | fbneo               | v1.0.0.03    |
| arcade/mame  | mame                | 0.244        |
| arcade/naomi | flycast             | Git          |
| atari2600    | stella              | 6.6          |
| atari7800    | prosystem           | 1.3e         |
| c64          | vice_x64            | 3.5          |
| dreamcast    | flycast             | Git          |
| gba          | mgba                | ?            |
| kodi         | -                   | -            |
| mastersystem | genesis_plus_gx     | v1.7.4       |
| megadrive    | genesis_plus_gx     | v1.7.4       |
| msx          | bluemsx             | SVN          |
| n64          | mupen64plus_next    | 1.0          |
| naomi        | flycast             | Git          |
| neocd        | neocd               | 2019         |
| neogeo       | fbneo               | v1.0.0.03    |
| nes          | fceumm              | SVN          |
| ngp          | mednafen_ngp        | v0.9.36.1    |
| pc           | dosbox_pure         | 0.9.7        |
| pcengine     | mednafen_supergrafx | v1.23.0      |
| pcenginecd   | mednafen_supergrafx | v1.23.0      |
| ports        | -                   | -            |
| psx          | swanstation         | v0.1         |
| scripts      | -                   | -            |
| sega32x      | picodrive           | 1.99         |
| segacd       | genesis_plus_gx     | v1.7.4       |
| sg1000       | genesis_plus_gx     | v1.7.4       |
| sgb          | mgba                | ?            |
| snes         | snes9x              | 1.61         |
| x86000       | px68k               | 0.15+        |
| zxspectrum   | fuse                | 1.6.0        |
