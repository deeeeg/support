OS4 Troubleshooting

In Progress

Rom/RetroArch Troubleshooting
    
- Black Screen When launching a game then it goes back to OS4

    - Check that the BIOS pack is installed
    - Use Rom from Rom pack
    - (Advanced) Enable logs by modifying /root/.config/retroarch/retroarch.cfg as seen below. 
        - libretro_log_level = "0/DEBUG"
        - log_to_file = "true"
        - log_to_file_timestamp = "true"

    The log will be located in /opt/rgbpi/ui/logs
