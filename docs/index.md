# RGB-Pi Community Wiki

Welcome to the unofficial RGB-Pi community wiki. We are arcade fans, just like you. This wiki was created as a tool to help you setup and configure the RGB-Pi hardware. If this wiki doesn't help you resolve the issue you are having, use the community [Issues](https://gitlab.com/rgb-pi/support/-/issues). 

Please consider [donating](https://ko-fi.com/rtomas/shop) to the creator of [RGB-Pi](https://www.rgb-pi.com/). Let's keep him continuing to develop great software.

## What is the RGB-Pi?

The RGB-Pi is a HAT (Hardware Attached on Top) for the Raspberry Pi (Low Cost Single Board Computer). It's function is to output pixel perfect RGB video signals to CRT TV's and arcade monitors via a scart or jamma.
