# RGB-Pi Community Support

Welcome to the unofficial RGB-Pi community [Wiki](https://rgb-pi.gitlab.io/support/) and [Issues](https://gitlab.com/rgb-pi/support/-/issues). We are arcade fans, just like you. This repo was created as a tool to help you setup and configure the RGB-Pi hardware.

Please consider [donating](https://ko-fi.com/rtomas/shop) to the creator of [RGB-Pi](https://www.rgb-pi.com/). Let's keep him continuing to develop great software.

## Official RGB-Pi Links

* [RGB-Pi](https://www.rgb-pi.com/)
* [Hardware](https://www.mortaca.com/)
* [Software](https://ko-fi.com/rtomas/shop)

## Getting Started

* [Community Wiki](https://rgb-pi.gitlab.io/support/)
* [Community Issues](https://gitlab.com/rgb-pi/support/-/issues)
